#include <Rcpp.h>
#include <fstream>
#include "wrap.h"

using namespace Rcpp;

void fix_names(RObject& x) {
    if (not x.hasAttribute("names")) return;
    std::vector<std::string> nms = x.attr("names");
    auto fix = [](std::string& s) {
        std::replace(s.begin(), s.end(), '/', '.');
        s.erase(0, 1);
        return s;
    };
    std::transform(nms.begin(), nms.end(), nms.begin(), fix);
    x.attr("names") = nms;
}

// [[Rcpp::export]]
RObject parse_json_impl(const std::string& input, bool fromfile = false, bool flatten = false) {
    json j = fromfile ? json::parse(std::ifstream(input)) : json::parse(input);
    RObject res = flatten ? j.flatten() : j;
    if (flatten) fix_names(res);
    return res;
}

// [[Rcpp::export]]
std::string dump_json_impl(const RObject& x, int ident = -1) {
    return json(x).dump(ident);
}

// [[Rcpp::export]]
RObject get_info_impl() {
    return json::meta();
}
