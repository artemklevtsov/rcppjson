#ifndef WRAP_H
#define WRAP_H

// [[Rcpp::plugins("cpp11")]]

#include <Rcpp.h>
#include <json.hpp>

using nlohmann::json;
using value_t = nlohmann::json::value_t;
using namespace Rcpp;

template<int RTYPE>
Vector<RTYPE> list2vec(const List& x) {
    return Vector<RTYPE>(x.begin(), x.end());
}

template<>
Vector<STRSXP> list2vec(const List& x) {
    std::size_t n = x.size();
    CharacterVector res = no_init(n);
    for (std::size_t i = 0; i < n; ++i) {
        if (TYPEOF(x[i]) == INTSXP && x[i] == NA_INTEGER)
            res[i] = NA_STRING;
        else
            res[i] = as<String>(x[i]);
    }
    return res;
}

template <typename VecType, typename ElemType = typename VecType::value_type>
void vec2json(json& jobj, const RObject robj) {
    std::size_t n = Rf_length(robj);
    VecType rtmp = as<VecType>(robj);
    if (n == 1) {
        if (VecType::is_na(rtmp[0]))
            jobj = nullptr;
        else
            jobj = static_cast<ElemType>(rtmp[0]);
    } else if (Rf_isMatrix(robj)) {
        std::size_t ncols = Rf_ncols(robj);
        std::size_t nrows = Rf_nrows(robj);
        for (std::size_t i = 0; i < nrows; ++i) {
            json::array_t jtmp;
            for (std::size_t j = 0; j < ncols; ++j) {
                if (VecType::is_na(rtmp[ncols * j + i]))
                    jtmp.emplace_back(nullptr);
                else
                    jtmp.emplace_back(static_cast<ElemType>(rtmp[nrows * j + i]));
            }
            jobj.emplace_back(jtmp);
        }
    } else {
        for (std::size_t i = 0; i < n; ++i) {
            if (VecType::is_na(rtmp[i]))
                jobj.emplace_back(nullptr);
            else
                jobj.emplace_back(static_cast<ElemType>(rtmp[i]));
        }
    }
}

json get_elem(const RObject& robj, std::size_t i) {
    json res;
    switch(robj.sexp_type()) {
        case INTSXP: {
            IntegerVector rtmp = as<IntegerVector>(robj);
            if (IntegerVector::is_na(rtmp[i])) {
                res = nullptr;
            } else if (Rf_isFactor(robj)) {
                std::vector<std::string> lvls = robj.attr("levels");
                res = lvls[rtmp[i] - 1];
            } else {
                res = rtmp[i];
            }
            break;
        }
        case REALSXP: {
            NumericVector rtmp = as<NumericVector>(robj);
            if (NumericVector::is_na(rtmp[i]))
                res = nullptr;
            else
                res = rtmp[i];
            break;
        }
        case LGLSXP: {
            LogicalVector rtmp = as<LogicalVector>(robj);
            if (LogicalVector::is_na(rtmp[i]))
                res = nullptr;
            else
                res = static_cast<bool>(rtmp[i]);
            break;
        }
        case STRSXP: {
            CharacterVector rtmp = as<CharacterVector>(robj);
            if (CharacterVector::is_na(rtmp[i]))
                res = nullptr;
            else
                res = as<std::string>(rtmp[i]);
            break;
        }
        default: stop("Unsupported type '%s'.", type2name(robj));
    }
    return res;
}

namespace nlohmann {
    template <>
    struct adl_serializer<RObject> {
        static void from_json(const json& j, RObject& r) {
            switch(j.type()) {
                case value_t::null: {
                    r = R_NilValue;
                    break;
                }
                case value_t::boolean: {
                    r = j.get<bool>();
                    break;
                }
                case value_t::number_integer: {
                    r = j.get<int64_t>();
                    break;
                }
                case value_t::number_unsigned: {
                    r = j.get<uint64_t>();
                    break;
                }
                case value_t::number_float: {
                    r = j.get<double>();
                    break;
                }
                case value_t::string: {
                    r = j.get<std::string>();
                    break;
                }
                case value_t::object: {
                    std::size_t n = j.size();
                    List res = no_init(n);
                    CharacterVector nms = no_init(n);
                    auto begin = j.begin(), end = j.end();
                    std::size_t i = 0;
                    for (auto it = begin; it != end; ++it, ++i) {
                        nms[i] = it.key();
                        res[i] = it.value().get<RObject>();
                    }
                    res.attr("names") = nms;
                    r = res;
                    break;
                }
                case value_t::array: {
                    std::size_t n = j.size();
                    List res = no_init(n);
                    bool is_vec = true;
                    bool is_mat = true;
                    bool is_Frame = true;
                    value_t type = value_t::null;
                    for (std::size_t i = 0; i < n; ++i) {
                        auto val = j[i];
                        res[i] = val.get<RObject>();
                        if (is_vec) {
                            if (not val.is_primitive())
                                is_vec = false;
                            type = std::max(type, j[i].type());
                        }
                    }
                    if (is_vec) {
                        switch(type) {
                            case value_t::null:
                            case value_t::boolean: {
                                r = list2vec<LGLSXP>(res);
                                break;
                            }
                            case value_t::number_unsigned:
                            case value_t::number_integer:
                            case value_t::number_float: {
                                r = list2vec<REALSXP>(res);
                                break;
                            }
                            case value_t::string: {
                                r = list2vec<STRSXP>(res);
                                break;
                            }
                            default: r = res;
                        }
                    } else {
                        r = res;
                    }
                    break;
                }
                default: stop("Unsupported type '%s'.", j.type_name());
            }
        }
        static void to_json(json& j, const RObject& r) {
            std::size_t n = Rf_length(r);
            switch(r.sexp_type()) {
                case NILSXP: {
                    j = nullptr;
                    break;
                }
                case LGLSXP: {
                    vec2json<LogicalVector, bool>(j, r);
                    break;
                }
                case INTSXP: {
                    vec2json<IntegerVector>(j, r);
                    break;
                }
                case REALSXP: {
                    vec2json<NumericVector>(j, r);
                    break;
                }
                case STRSXP: {
                    vec2json<CharacterVector, std::string>(j, r);
                    break;
                }
                case VECSXP: {
                    List t = as<List>(r);
                    if (r.hasAttribute("names")) {
                        std::vector<std::string> nms = r.attr("names");
                        if (Rf_isFrame(r)) {
                            std::size_t ncols = n;
                            std::size_t nrows = Rf_length(t[0]);
                            for (std::size_t i = 0; i < nrows; ++i) {
                                json::object_t jj;
                                for (std::size_t j = 0; j < ncols; ++j)
                                    jj[nms[j]] = get_elem(t[j], i);
                                j.emplace_back(jj);
                            }
                        } else {
                            for (std::size_t i = 0; i < n; ++i) {
                                if (nms[i].empty())
                                    nms[i] = std::to_string(i + 1);
                                j[nms[i]] = static_cast<RObject>(t[i]);
                            }
                        }
                    } else {
                        for (std::size_t i = 0; i < n; ++i)
                            j.emplace_back(static_cast<RObject>(t[i]));
                    }
                    break;
                }
                default: stop("Unsupported type '%s'", type2name(r));
            }
        }
    }; // struct adl_serializer
} // namespace nlohmann

#endif
