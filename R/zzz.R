.onAttach <- function(libname, pkgname) {
    if (interactive()) {
        pkg_info <- get_info()
        packageStartupMessage(paste0("<", pkg_info$name, ">"))
        packageStartupMessage(sprintf("  Compiler: %s %s (c++ %s)", pkg_info$compiler$family, pkg_info$compiler$version, pkg_info$compiler$`c++`))
        packageStartupMessage(paste("  Platform:", pkg_info$platform))
        packageStartupMessage(paste("  Version", pkg_info$version$string))
        packageStartupMessage(paste("  URL", pkg_info$url))
        packageStartupMessage(paste("  Copyright", pkg_info$copyright))
    }
}
