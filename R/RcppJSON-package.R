#' @title RcppJSON --- Rcpp bindings to JSON via JSON for Modern C++
#'
#' @description
#' JSON (JavaScript Object Notation) is a lightweight data-interchange format.
#' It is easy for humans to read and write. It is easy for machines to parse
#' and generate. It is based on a subset of the JavaScript Programming Language,
#' Standard ECMA-262 3rd Edition - December 1999. JSON is a text format that is
#' completely language independent but uses conventions that are familiar to
#' programmers of the C-family of languages, including C, C++, C#, Java,
#' JavaScript, Perl, Python, and many others. These properties make JSON an
#' ideal data-interchange language.
#'
#' @name RcppJSON
#'
#' @docType package
#'
#' @author Artem Klevtsov <\email{a.a.klevtsov@gmail.com}>
#'
#' @references
#' \href{https://github.com/nlohmann/json}{JSON for Modern C++}
#' \href{https://gitlab.com/artemklevtsov/rcppjson}{RcppJSON package}
#' \href{http://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf}{The JSON Data Interchange Format}
#'
NULL

#' @useDynLib RcppJSON, .registration=TRUE
#' @importFrom Rcpp sourceCpp
NULL
