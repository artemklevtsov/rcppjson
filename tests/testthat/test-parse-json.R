context("Parse JSON from string")

test_that("Parse null value", {
    expect_equal(parse_json_impl('null'), NA_integer_)
})

test_that("Parse logical value", {
    expect_equal(parse_json_impl('true'), TRUE)
    expect_equal(parse_json_impl('false'), FALSE)
})

test_that("Parse integer value", {
    expect_equal(parse_json_impl('1'), 1L)
    expect_equal(parse_json_impl('-1'), -1L)
})

test_that("Parse double value", {
    expect_equal(parse_json_impl('1.0'), 1.0)
})

test_that("Parse string value", {
    expect_equal(parse_json_impl('"s"'), "s")
})

test_that("Parse null array", {
    expect_equal(parse_json_impl('[null, null]'), c(NA, NA))
})

test_that("Parse integer array", {
    expect_equal(parse_json_impl('[1, 2, 3]'), c(1L, 2L, 3L))
    expect_equal(parse_json_impl('[1, 2, null]'), c(1L, 2L, NA))
})

test_that("Parse double array", {
    expect_equal(parse_json_impl('[1, 2.1, 3]'), c(1, 2.1, 3))
    expect_equal(parse_json_impl('[1, 2.1, null]'), c(1, 2.1, NA))
})

test_that("Parse double array", {
    expect_equal(parse_json_impl('[true, false, false]'), c(TRUE, FALSE, FALSE))
    expect_equal(parse_json_impl('[true, false, null]'), c(TRUE, FALSE, NA))
})

test_that("Parse string array", {
    expect_equal(parse_json_impl('["a", "b", "c"]'), c("a", "b", "c"))
    expect_equal(parse_json_impl('["a", "b", null]'), c("a", "b", NA))
})

test_that("Parse flat object", {
    i <- parse_json_impl('{"a": 1, "b": 2.1, "c": "string", "d": null}')
    o <- list(a = 1, b = 2.1, c = "string", d = NA_integer_)
    expect_equal(i, o)
})

test_that("Parse nested object", {
    i <- parse_json_impl('{"a": 1.0, "b": true, "c": null, "d": [1, true, "s"], "e": [true, false, null]}')
    o <- list(a = 1, b = TRUE, c = NA_integer_, d = list(1, TRUE, "s"), e = c(TRUE, FALSE, NA))
    expect_equal(i, o)
})

test_that("Parse nested object fith flatten = TRUE", {
    i <- parse_json_impl('{"a": 1.0, "b": true, "c": null, "d": [1, true, "s"], "e": [true, false, null]}', flatten = TRUE)
    o <- list(a = 1, b = TRUE, c = NA_integer_, d.0 = 1, d.1 = TRUE,  d.2 = "s", e.0 = TRUE, e.1 = FALSE, e.2 = NA_integer_)
    expect_equal(i, o)
})
