context("Dump JSON")

test_that("Dump NULL", {
    expect_equal(dump_json_impl(NULL), 'null')
})

test_that("Dump NA value", {
    expect_equal(dump_json_impl(NA), 'null')
    expect_equal(dump_json_impl(NA_character_), 'null')
    expect_equal(dump_json_impl(NA_integer_), 'null')
    expect_equal(dump_json_impl(NA_real_), 'null')
})

test_that("Dump logical value", {
    expect_equal(dump_json_impl(TRUE), 'true')
    expect_equal(dump_json_impl(FALSE), 'false')
})

test_that("Dump integer value", {
    expect_equal(dump_json_impl(1L), '1')
})

test_that("Dump double value", {
    expect_equal(dump_json_impl(1), '1.0')
    expect_equal(dump_json_impl(1.0), '1.0')
})

test_that("Dump string", {
    expect_equal(dump_json_impl("string"), '"string"')
})

test_that("Dump logical vector", {
    expect_equal(dump_json_impl(c(TRUE, FALSE)), '[true,false]')
    expect_equal(dump_json_impl(c(TRUE, FALSE, NA)), '[true,false,null]')
})

test_that("Dump integer vector", {
    expect_equal(dump_json_impl(c(1L, 2L)), '[1,2]')
    expect_equal(dump_json_impl(c(1L, 2L, NA)), '[1,2,null]')
})

test_that("Dump double vector", {
    expect_equal(dump_json_impl(c(1, 2)), '[1.0,2.0]')
    expect_equal(dump_json_impl(c(1.0, 2.0)), '[1.0,2.0]')
    expect_equal(dump_json_impl(c(1, 2, NA)), '[1.0,2.0,null]')
})

test_that("Dump character vector", {
    expect_equal(dump_json_impl(c("a", "b")), '["a","b"]')
    expect_equal(dump_json_impl(c("a", "b", NA)), '["a","b",null]')
})

test_that("Dump unnamed list", {
    expect_equal(dump_json_impl(list(1, 2L, "a")), '[1.0,2,"a"]')
    expect_equal(dump_json_impl(list(1, 2, 3)), "[1.0,2.0,3.0]")
})

test_that("Dump named list", {
    expect_equal(dump_json_impl(list(a = 1, b = 2, c = TRUE)), '{"a":1.0,"b":2.0,"c":true}')
})

test_that("Dump partial named list", {
    expect_equal(dump_json_impl(list(a = 1, b = 2, 3, c = TRUE)), '{"3":3.0,"a":1.0,"b":2.0,"c":true}')

})

test_that("Dump data.frame", {
    d <- data.frame(a = 1:2, b = c("a", "b"))
    expect_equal(dump_json_impl(d), '[{"a":1,"b":"a"},{"a":2,"b":"b"}]')
})
